<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class ControllerDNI extends Controller {

    public function index() {
        helper(['form', 'url']);

        if ($this->request->getVar('dni')!==null)([
                !$this->validate
                
                    'dni' => ['label' => 'DNI', 'errors'=>
                    ['valid_dni' => 'El DNI introducido no es válido','required' => 'Por favor, ingresa un DNI en el campo'],
                    'rules' => 'required|valid_dni'],
                ])
                ) {
            echo view('Signup', [
                'validation' => $this->validator,
            ]);
        } else 
            echo view('Success');
        }
    }



